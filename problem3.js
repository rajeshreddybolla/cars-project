var cars = require('./cars').inventory;


function carSort(cars , model) {
    cars.sort (
        function (a, b) {
            if (a[model] < b[model]){
                return -1;
            } else if (a[model] > b[model]){
                return 1;
            } else {
                return 0;   
            }
        }
    );
}

exports.carSort = carSort;

